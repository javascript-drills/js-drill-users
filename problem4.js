// Q4 Group users based on their Programming language mentioned in their designation.

const pLanguages = ["Golang", "Javascript", "Python", "Java", "C++", "Rust"];

function searchLang( langDataset, userLang ){
    for( let index=0; index<langDataset.length; index++ ){
        if(langDataset[index] == userLang){
            return true;
        }
    }
    return false;
}

function groupUsers( users ){
    const userGroupByLang = {};

    for( let userName in users ){
        const userData = users[userName];
        const userDesignation = userData.desgination.split(" ");

        for( let index=0; index<userDesignation.length; index++ ){
            let newLang = userDesignation[index];
            
            if(searchLang(pLanguages, newLang)){
                if(!userGroupByLang[newLang]){
                    userGroupByLang[newLang] = [];
                }
                userGroupByLang[newLang].push(userName);
            }
        }
    }
    return userGroupByLang;
}

module.exports = { groupUsers };