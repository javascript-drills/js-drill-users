// Q2 Find all users staying in Germany.

function usersInGermany(users){
    let flag = 0;
    for( let userNames in users ){
        const userData = users[userNames];

        if(userData.nationality == "Germany"){
            console.log(userNames);
            flag = 1;
        }
    }
    return flag;
}
module.exports = { usersInGermany };