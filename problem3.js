// Q3 Find all users with masters Degree.

function mastersQualified( users ){
    let flag = 0;
    for( let userName in users ){
        const userData = users[userName];

        if( userData.qualification == "Masters" ){
            console.log(userName);
            flag = 1;
        }
    }
    return flag;
}

module.exports = { mastersQualified };