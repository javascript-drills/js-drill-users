// Q1 Find all users who are interested in playing video games.

function getVideoGameUsers( users ){
    const interestedInVideoGames = [];

    for( let userName in users ){
        const userData = users[userName];

        let interests = userData.interests || userData.interest;

        interests = interests[0].split(", ");

        for( let index=0; index<interests.length; index++){
            if(interests[index].includes("Video Games")){
                interestedInVideoGames.push(userName);
            }
        }
    }
    return interestedInVideoGames;
}

module.exports = { getVideoGameUsers };